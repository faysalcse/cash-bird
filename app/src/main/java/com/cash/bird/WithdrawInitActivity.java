package com.cash.bird;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;

public class WithdrawInitActivity extends Activity {

    TextView totalCoinsWDInitTextView;
    TextView alertTitle;
    EditText editTextPayPalEmail;
    EditText editTextPayPalConfirmEmail;
    EditText withdrawCoins;
    ImageView plogo;
    float available_coins;
    ProgressDialog dialog;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private FirebaseFirestore firebaseFirestore;

    Dialog noInternetDialog;
    Button closeButton, turnOnButton;
    String method;

    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw_init);

        plogo = findViewById(R.id.plogo);
        alertTitle = findViewById(R.id.alertTitle);
        totalCoinsWDInitTextView = findViewById(R.id.totalCoinsWDInitTextView);
        editTextPayPalEmail = findViewById(R.id.editTextPayPalEmail);
        editTextPayPalConfirmEmail = findViewById(R.id.editTextPayPalConfirmEmail);
        withdrawCoins = findViewById(R.id.withdrawCoins);
        noInternetDialog = new Dialog(this);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        firebaseFirestore = FirebaseFirestore.getInstance();

        Bundle bundle = getIntent().getExtras();
        available_coins = bundle.getFloat("available_coins");
        method = bundle.getString("method");
        totalCoinsWDInitTextView.setText(Float.toString(available_coins));


        if (method.equalsIgnoreCase("paypal")) {
            plogo.setImageResource(R.drawable.paypal);
            alertTitle.setText("Enter your PayPal Email ID to Process Payment");
            editTextPayPalEmail.setHint("Email Id");
            editTextPayPalConfirmEmail.setHint("Confirm Email ID");
        } else if (method.equalsIgnoreCase("cashu")) {
            alertTitle.setText("Enter your Phone Number to Process Payment");
            editTextPayPalEmail.setHint("Phone Number");
            editTextPayPalConfirmEmail.setHint("Confirm Phone Number");
            plogo.setImageResource(R.drawable.cashu);
        } else if (method.equalsIgnoreCase("google")) {
            alertTitle.setText("Enter your Google Gift Card Email ID to Process Payment");
            editTextPayPalEmail.setHint("Google Gift Card Email ID");
            editTextPayPalConfirmEmail.setHint("Confirm Google Gift Card Email ID");
            plogo.setImageResource(R.drawable.google_gift__card);
        } else if (method.equalsIgnoreCase("PUBG")) {
            alertTitle.setText("Enter your Email ID to Process Payment");
            editTextPayPalEmail.setHint("Email ID");
            editTextPayPalConfirmEmail.setHint("Confirm Email ID");
            plogo.setImageResource(R.drawable.pubg);
        }else if (method.equalsIgnoreCase("iTunes")) {
            alertTitle.setText("Enter your Email ID to Process Payment");
            editTextPayPalEmail.setHint("Email ID");
            editTextPayPalConfirmEmail.setHint("Confirm Email ID");
            plogo.setImageResource(R.drawable.itunes);
        } else {
            alertTitle.setText("Enter your Garena Free Fire Player ID Process Payment");
            editTextPayPalEmail.setHint("Garena Free Fire Player ID");
            editTextPayPalConfirmEmail.setHint("Confirm Garena Free Fire Player ID");
            plogo.setImageResource(R.drawable.garena);
        }

        dialog = new ProgressDialog(WithdrawInitActivity.this);
        dialog.setMessage("Processing Request...");
        dialog.setCancelable(false);

    }

    public void withdrawConfirmButton(View view) {
        if (haveNetworkConnection()) {
            if (checkForFieldClearance()) {
                float withdrawCoinsEntered = Float.parseFloat(withdrawCoins.getText().toString());
                if (withdrawCoinsEntered > available_coins) {
                    Toast.makeText(this, "Invalid Amount: Exceeding Available Coins.", Toast.LENGTH_LONG).show();
                } else if (withdrawCoinsEntered > 0 && withdrawCoinsEntered <= available_coins) {
                    //Initiate Final Withdraw and Update Database
                    DeductAndSaveToDB(withdrawCoinsEntered, available_coins);
                } else {
                    Toast.makeText(this, "Invalid Request Amount.", Toast.LENGTH_LONG).show();
                }
            }
        } else {
            // display No Internet Connected
            showNoInternetToUser();
        }

    }

    private void showNoInternetToUser() {
        noInternetDialog.setContentView(R.layout.popup_nointernet_withdraw);
        closeButton = noInternetDialog.findViewById(R.id.closeButton);
        turnOnButton = noInternetDialog.findViewById(R.id.noInternetTurnOnButtonWithdraw);
        turnOnButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                noInternetDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Turn On Internet Service", Toast.LENGTH_LONG).show();
            }
        });
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                noInternetDialog.dismiss();
            }
        });
        noInternetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        noInternetDialog.setCancelable(false);
        noInternetDialog.show();
    }

    private void DeductAndSaveToDB(float withdrawCoinsEntered, float available_coins) {
        dialog.show();
        float updatedTotalCoins = Math.abs(available_coins - withdrawCoinsEntered);


        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        numberFormat.setMinimumFractionDigits(2);
        numberFormat.setMaximumFractionDigits(2);
        final String updatedTotalCoins_ = numberFormat.format(updatedTotalCoins);

        Map<String, Object> userProfile_update = new HashMap<>();
        userProfile_update.put("user_earned_coins", updatedTotalCoins);




        final Map<String, Object> userProfile_Add_ToWithdrawRequest = new HashMap<>();
        userProfile_Add_ToWithdrawRequest.put("user_uid", firebaseUser.getUid());
        userProfile_Add_ToWithdrawRequest.put("user_email", editTextPayPalEmail.getText().toString());
        userProfile_Add_ToWithdrawRequest.put("user_withdraw_coins", Float.toString(withdrawCoinsEntered));
        userProfile_Add_ToWithdrawRequest.put("request_timestamp", Timestamp.now());
        userProfile_Add_ToWithdrawRequest.put("status", "in_progress");
        userProfile_Add_ToWithdrawRequest.put("method", method);

        firebaseFirestore.collection("users")
                .document(firebaseUser.getUid())
                .update(userProfile_update)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            String unique_id = firebaseFirestore.collection("withdraw_requests").document().getId();
                            firebaseFirestore.collection("withdraw_requests")
                                    .document(unique_id)
                                    .set(userProfile_Add_ToWithdrawRequest);
                            dialog.dismiss();
                            showConfirmationToUser();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        showErrorWhileProcessing();
                    }
                });
    }

    private void showErrorWhileProcessing() // Withdraw Failed
    {
        noInternetDialog.setContentView(R.layout.popup_withdraw_failed);
        Button okayButton = noInternetDialog.findViewById(R.id.okayButton);
        okayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                noInternetDialog.dismiss();
                Intent intent = new Intent(WithdrawInitActivity.this, MainActivity.class);
                startActivity(intent);
                WithdrawInitActivity.this.finish();
            }
        });
        noInternetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        noInternetDialog.setCancelable(false);
        noInternetDialog.show();

    }

    private void showConfirmationToUser() // Withdraw Success
    {
        noInternetDialog.setContentView(R.layout.popup_withdraw_success);
        Button okayButton = noInternetDialog.findViewById(R.id.okayButton);
        okayButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                noInternetDialog.dismiss();
                Intent intent = new Intent(WithdrawInitActivity.this, MainActivity.class);
                startActivity(intent);
                WithdrawInitActivity.this.finish();
            }
        });
        noInternetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        noInternetDialog.setCancelable(false);
        noInternetDialog.show();
    }

    private boolean checkForFieldClearance() {
        if (method.equalsIgnoreCase("cashu")) {
            if (editTextPayPalEmail.getText().toString().isEmpty()) {
                Toast.makeText(this, "Phone Number field Can't be Empty", Toast.LENGTH_SHORT).show();
                return false;
            }
            if (editTextPayPalConfirmEmail.getText().toString().isEmpty()) {
                Toast.makeText(this, "Confirm Phone Number field Can't be Empty", Toast.LENGTH_SHORT).show();
                return false;
            }

            if (!editTextPayPalEmail.getText().toString().equalsIgnoreCase(editTextPayPalConfirmEmail.getText().toString())) {
                Toast.makeText(this, "Phone Number and Confirm Phone Number Doesn't Match", Toast.LENGTH_LONG).show();
                return false;
            }
            if (withdrawCoins.getText().toString().isEmpty()) {
                Toast.makeText(this, "Enter Number of Coins to Withdraw", Toast.LENGTH_LONG).show();
                return false;
            }
        } else if (method.equalsIgnoreCase("garena")) {
            if (editTextPayPalEmail.getText().toString().isEmpty()) {
                Toast.makeText(this, "Garena Free Fire Player ID field Can't be Empty", Toast.LENGTH_SHORT).show();
                return false;
            }
            if (editTextPayPalConfirmEmail.getText().toString().isEmpty()) {
                Toast.makeText(this, "Confirm Phone Garena Free Fire Player ID Can't be Empty", Toast.LENGTH_SHORT).show();
                return false;
            }

            if (!editTextPayPalEmail.getText().toString().equalsIgnoreCase(editTextPayPalConfirmEmail.getText().toString())) {
                Toast.makeText(this, "Garena Free Fire Player ID and Confirm Garena Free Fire Player ID Doesn't Match", Toast.LENGTH_LONG).show();
                return false;
            }
            if (withdrawCoins.getText().toString().isEmpty()) {
                Toast.makeText(this, "Enter Number of Coins to Withdraw", Toast.LENGTH_LONG).show();
                return false;
            }
        } else {
            if (editTextPayPalEmail.getText().toString().isEmpty()) {
                Toast.makeText(this, "Email ID field Can't be Empty", Toast.LENGTH_SHORT).show();
                return false;
            }
            if (editTextPayPalConfirmEmail.getText().toString().isEmpty()) {
                Toast.makeText(this, "Confirm Email ID field Can't be Empty", Toast.LENGTH_SHORT).show();
                return false;
            }
            if (!editTextPayPalEmail.getText().toString().matches(emailPattern)) {
                Toast.makeText(this, "Incorrect Email ID", Toast.LENGTH_SHORT).show();
                return false;
            }
            if (!editTextPayPalEmail.getText().toString().matches(emailPattern)) {
                Toast.makeText(this, "Incorrect Confirm Email ID", Toast.LENGTH_SHORT).show();
                return false;
            }
            if (!editTextPayPalEmail.getText().toString().equalsIgnoreCase(editTextPayPalConfirmEmail.getText().toString())) {
                Toast.makeText(this, "Email and Confirm Email Doesn't Match", Toast.LENGTH_LONG).show();
                return false;
            }
            if (withdrawCoins.getText().toString().isEmpty()) {
                Toast.makeText(this, "Enter Number of Coins to Withdraw", Toast.LENGTH_LONG).show();
                return false;
            }
        }


        return editTextPayPalEmail.getText().toString().equalsIgnoreCase(editTextPayPalConfirmEmail.getText().toString());
    }


    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

}
