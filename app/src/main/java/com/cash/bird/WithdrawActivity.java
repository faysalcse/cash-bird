package com.cash.bird;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.NumberFormat;
import java.util.Timer;
import java.util.TimerTask;


public class WithdrawActivity extends Activity {

    private static int ELIGIBLE_LIMIT = 1000;
    Button withdrawCheckButton;
    Button okayButtonNoCoins;
    TextView totalCoinsTextView;
    static float totalEarnedCoins;
    private Timer timer;
    AdsAuthorization adsAuthorization;

    ProgressDialog dialog;
    Dialog noCoinsDialog;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private FirebaseFirestore firebaseFirestore;
    NumberFormat numberFormat;
//    TextView withdrawOption1;
//    TextView withdrawOption2;
//    TextView withdrawOption3;
//    TextView withdrawOption4;
//    TextView withdrawOption5;
//
//    TextView withdrawOption1_value;
//    TextView withdrawOption2_value;
//    TextView withdrawOption3_value;
//    TextView withdrawOption4_value;
//    TextView withdrawOption5_value;
    private InterstitialAd interstitialAd;

    TextView points2money;

    private String method;

    ImageView plogo;


    public static final String TAG = GameOver.class.getSimpleName();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw);


        plogo = findViewById(R.id.plogo);

        Intent intent =getIntent();
        if (intent !=null){
            method = intent.getStringExtra("method");
            if (method.equalsIgnoreCase("paypal")){
                plogo.setImageResource(R.drawable.paypal);
            }else if (method.equalsIgnoreCase("cashu")){
                plogo.setImageResource(R.drawable.cashu);
            }else if (method.equalsIgnoreCase("google")){
                plogo.setImageResource(R.drawable.google_gift__card);
            }else if (method.equalsIgnoreCase("pubg")){
                plogo.setImageResource(R.drawable.pubg);
            }else if (method.equalsIgnoreCase("itunes")){
                plogo.setImageResource(R.drawable.itunes);
            }else{
                plogo.setImageResource(R.drawable.garena);
            }
        }



        totalCoinsTextView = findViewById(R.id.totalCoinsTextView);
        points2money = findViewById(R.id.points2money);
        withdrawCheckButton = findViewById(R.id.withdrawCheckButton);
        noCoinsDialog = new Dialog(this);
        adsAuthorization = new AdsAuthorization(this);

      /*  withdrawOption1 = findViewById(R.id.withdrawOption1);
        withdrawOption2 = findViewById(R.id.withdrawOption2);
        withdrawOption3 = findViewById(R.id.withdrawOption3);
        withdrawOption4 = findViewById(R.id.withdrawOption4);
        withdrawOption5 = findViewById(R.id.withdrawOption5);

        withdrawOption1_value = findViewById(R.id.withdrawOption1_value);
        withdrawOption2_value = findViewById(R.id.withdrawOption2_value);
        withdrawOption3_value = findViewById(R.id.withdrawOption3_value);
        withdrawOption4_value = findViewById(R.id.withdrawOption4_value);
        withdrawOption5_value = findViewById(R.id.withdrawOption5_value);*/

        // Initialize the Audience Network SDK
        MobileAds.initialize(this);

        numberFormat = NumberFormat.getNumberInstance();
        numberFormat.setMinimumFractionDigits(2);
        numberFormat.setMaximumFractionDigits(2);


        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        firebaseFirestore = FirebaseFirestore.getInstance();

        dialog = new ProgressDialog(WithdrawActivity.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);


        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(getResources().getString(R.string.admob_interstitial));
        interstitialAd.setAdListener(new AdListener(){
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                interstitialAd.show();
            }

            @Override
            public void onAdFailedToLoad(LoadAdError adError) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the interstitial ad is closed.
            }
        });

        interstitialAd.loadAd(new AdRequest.Builder().build());

        AdsAuthorizationTimer();
        updateEarnedCoins();

    }

    private void AdsAuthorizationTimer() {
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                AdsSetVisibility();
            }
        };
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                handler.post(runnable);
            }
        }, 3000);

    }

    private void AdsSetVisibility() {
        if (interstitialAd == null || !interstitialAd.isLoaded()) {
            return;
        }

        // Show the ad
        interstitialAd.show();

    }


    private void updateEarnedCoins() {
        dialog.show();
        firebaseFirestore.collection("users")
                .document(firebaseUser.getUid())
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            String TotalCoins=  String.valueOf(task.getResult().get("user_earned_coins"));
                            String EarnedCoins_ = numberFormat.format(Float.parseFloat(TotalCoins));
                            totalCoinsTextView.setText(EarnedCoins_);
                            updateTotalEarnedCoins(TotalCoins);
                            updateWithdrawOptions();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Failed To Fetch Details, Try Again With Good Network", Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void updateTotalEarnedCoins(String totalCoins) {
        WithdrawActivity.totalEarnedCoins = Float.parseFloat(totalCoins);
    }

    private void updateWithdrawOptions() {
        firebaseFirestore.collection("admin")
                .document("withdraw_options")
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {



                            String pointsToMoney = task.getResult().getString(method.toLowerCase()+"").replaceAll( "\\\\n", "\n" );

                            points2money.setText(pointsToMoney);

                            updateEligibleLimit();


                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Failed To Fetch Details, Try Again With Good Network", Toast.LENGTH_LONG).show();
                    }
                });


    }

    private void updateEligibleLimit() {
        firebaseFirestore.collection("admin")
                .document("withdraw_eligibility")
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (task.isSuccessful()) {
                            String eligible_limit = task.getResult().getString("limit");
                            updateStaticVariable(eligible_limit);
                            dialog.dismiss();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        dialog.dismiss();
                        Toast.makeText(getApplicationContext(), "Failed To Fetch Details, Try Again With Good Network", Toast.LENGTH_LONG).show();
                    }
                });

    }

    private void updateStaticVariable(String eligible_limit) {
        ELIGIBLE_LIMIT = Integer.parseInt(eligible_limit);

        // Toast.makeText(getApplicationContext(),""+ELIGIBLE_LIMIT,Toast.LENGTH_SHORT).show();
    }

    public void checkIsWithdrawEligible(View view) {
        float totalCoins = totalEarnedCoins;
        if (totalCoins >= ELIGIBLE_LIMIT) {
            Intent intent = new Intent(this, WithdrawInitActivity.class);
            intent.putExtra("available_coins", totalCoins);
            intent.putExtra("method",method);
            startActivity(intent);
        } else {
            //Show Dialog (Not Enough Coins to Cash Out)
            noCoinsDialog.setContentView(R.layout.popup_nocoins_towithdraw);
            okayButtonNoCoins = noCoinsDialog.findViewById(R.id.okayButtonNoCoins);
            okayButtonNoCoins.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    noCoinsDialog.dismiss();
                }
            });
            noCoinsDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            noCoinsDialog.setCancelable(false);
            noCoinsDialog.show();
            //Toast.makeText(this,"No Enough Coins To Cash Out.",Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onDestroy() {
        WithdrawActivity.this.finish();
        super.onDestroy();
    }
}
