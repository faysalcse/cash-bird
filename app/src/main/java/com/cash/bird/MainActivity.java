package com.cash.bird;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdCallback;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

//import android.support.annotation.NonNull;
//import android.support.annotation.Nullable;
//import android.support.v7.app.AppCompatActivity;

public class MainActivity extends Activity {

    private AdView adView;
    ImageButton imageBird, rewardVideoImage;
    int[] bird, rewardVdImg;
    private Timer timer;
    int currentFrame =0 , currentFrameRewardVDImg = 0;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private FirebaseFirestore firebaseFirestore;
    private FirebaseRemoteConfig firebaseRemoteConfig;
    Dialog noInternetDialog, rewardedVideoDialog, coinsAddSuccessDialog;
    Button itsOkayButton;
    Button turnOnButton;
    Button noInternetTurnOnButtonWithdraw;
    Button closeButton;

    Button watchNowButton;
    TextView RVCurrentCountTextView;

    AdsAuthorization adsAuthorization;
    private HashMap<String, Object> firebaseDefaults;
    private final String LATEST_APP_VERSION_CODE = "latest_version_code";
    private final String REWARD_VIDEO_LIMIT = "reward_video_daily_limit";
    LinearLayout adContainer;
    ProgressDialog dialog;

    int rewardVdCurrentCount = 0;
    int rewardVdLimit_RemoteConfig = 5; // update this variable from firebase remote configuration

    private final String TAG = "REWARDED_VIDEO_TAG";
    private RewardedAd rewardedVideoAd;
    private AdRequest adRequest;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MobileAds.initialize(this);

        FirebaseApp.initializeApp(this);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        firebaseFirestore = FirebaseFirestore.getInstance();


        adsAuthorization= new AdsAuthorization(this);

        noInternetDialog = new Dialog(this);
        rewardedVideoDialog = new Dialog(this);
        coinsAddSuccessDialog =new Dialog(this);

        getCurrentUserVariables();

        imageBird= findViewById(R.id.imageBird);
        rewardVideoImage = findViewById(R.id.rewardVideoImage);

        bird = new int[8];
        bird[0] = R.drawable.bird_frame1;
        bird[1] = R.drawable.bird_frame2;
        bird[2] = R.drawable.bird_frame3;
        bird[3] = R.drawable.bird_frame4;
        bird[4] = R.drawable.bird_frame5;
        bird[5] = R.drawable.bird_frame6;
        bird[6] = R.drawable.bird_frame7;
        bird[7] = R.drawable.bird_frame8;

        rewardVdImg = new int[4];
        rewardVdImg[0] = R.drawable.c1;
        rewardVdImg[1] = R.drawable.c2;
        rewardVdImg[2] = R.drawable.c3;
        rewardVdImg[3] = R.drawable.c4;

        AnimationHandlerInitiate();
        saveUserRecentOnline();
        isFacebookInstalled();
        AppConstants.initialization(this.getApplicationContext());
        // Your app needs to call MobileAds.initialize() method only once,
        // ideally in launcher activity. In our case MainActivity.
        // There's no need to call it twice during a single execution of the app,
        // or in every single activity.
        // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713
        // Replace this with your app ID

        //Toast.makeText(this,AppConstants.SCREEN_WIDTH + " : " +AppConstants.SCREEN_HEIGHT ,Toast.LENGTH_SHORT).show();

        OnstartPriority();

        adView = findViewById(R.id.adView);
         adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        dialog = new ProgressDialog(MainActivity.this);
        dialog.setMessage("Loading Video...");
        dialog.setCancelable(false);

        loadBannerAd();
        AdsAuthorizationTimer();
    }

    private void isFacebookInstalled()
    {
        String packageName="com.facebook.katana";
        PackageManager pm = this.getPackageManager();
        try {
            pm.getPackageInfo(packageName, 0);
            updateFirebase_FB(true);
        } catch (PackageManager.NameNotFoundException e) {
            updateFirebase_FB(false);
        }
    }

    private void updateFirebase_FB(boolean b)
    {
        if(firebaseUser!=null)
        {
            Map<String, Object> userProfile = new HashMap<>();
            userProfile.put("user_facebook_installed", Boolean.toString(b));
            firebaseFirestore.collection("users")
                    .document(firebaseUser.getUid())
                    .update(userProfile);
        }
    }

    private void loadBannerAd()
    {

        adView.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(LoadAdError adError) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });

    }

    private void getCurrentUserVariables()
    {
        if(firebaseUser!=null)
        {
            firebaseFirestore.collection("users")
                    .document(firebaseUser.getUid())
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                            if(task.isSuccessful())
                            {
                                try
                                {
                                   String temp = task.getResult().getString("daily_reward_count");
                                    if(temp == null)
                                    {
                                        Map<String, Object> updateUserProfile = new HashMap<>();
                                        updateUserProfile.put("daily_reward_count","0");
                                        firebaseFirestore.collection("users")
                                                .document(firebaseUser.getUid())
                                                .update(updateUserProfile);
                                       // Toast.makeText(getApplicationContext(),"created",Toast.LENGTH_SHORT).show();
                                        getCurrentUserVariables();
                                    }
                                    else
                                    {
                                        int dailyRewardCurrentCount = Integer.parseInt(temp);
                                        updateVariableCurrentCount(dailyRewardCurrentCount);
                                        //Toast.makeText(getApplicationContext(),"Fetched Daily Count: "+ dailyRewardCurrentCount,Toast.LENGTH_SHORT).show();
                                    }
                                }
                                catch (NullPointerException e)
                                {
                                    Map<String, Object> updateUserProfile = new HashMap<>();
                                    updateUserProfile.put("daily_reward_count","0");
                                    firebaseFirestore.collection("users")
                                    .document(firebaseUser.getUid())
                                    .update(updateUserProfile);
                                    //Toast.makeText(getApplicationContext(),"created",Toast.LENGTH_SHORT).show();
                                    getCurrentUserVariables();
                                }
                                catch (Exception ignored){}

                            }
                        }
                    });
        }
    }

    private void updateVariableCurrentCount(int dailyRewardCurrentCount)
    {
        this.rewardVdCurrentCount = dailyRewardCurrentCount;
    }

    private void remoteConfigCheck()
    {
        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        firebaseDefaults = new HashMap<>();
        firebaseDefaults.put(LATEST_APP_VERSION_CODE,getCurrentVersionCode());
        firebaseDefaults.put(REWARD_VIDEO_LIMIT,5);
        firebaseRemoteConfig.setDefaults(firebaseDefaults);
        firebaseRemoteConfig.setConfigSettings(new FirebaseRemoteConfigSettings.Builder().setDeveloperModeEnabled(true).build());

        firebaseRemoteConfig.fetch().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful())
                {
                    firebaseRemoteConfig.activateFetched();
                    checkForUpdates();
                }
            }
        });
    }

    private int getCurrentVersionCode()
    {
         try {
                return getPackageManager().getPackageInfo(getPackageName(),0).versionCode;
         }
         catch (PackageManager.NameNotFoundException e)
         {
             e.printStackTrace();
         }
        return -1;
    }

    private void checkForUpdates()
    {
        final String packageName = this.getPackageName();
        int fetchedVersionCode = (int) firebaseRemoteConfig.getDouble(LATEST_APP_VERSION_CODE);

        //variable updated with video limit set
        rewardVdLimit_RemoteConfig = (int) firebaseRemoteConfig.getDouble(REWARD_VIDEO_LIMIT);

        if(getCurrentVersionCode()<fetchedVersionCode)
        {
            AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
            builder1.setTitle("New Update Available...");
            builder1.setMessage("Update to the latest version of the game is strongly recommended.");
            builder1.setCancelable(false);

            builder1.setPositiveButton(
                    "Update Now",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.dismiss();
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + packageName)));
                            }
                            catch (Exception e)
                            { }
                        }
                    });

            builder1.setNegativeButton(
                    "Dismiss",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            MainActivity.this.finish();
                            System.exit(0);
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
        }
    }

    private void saveUserRecentOnline()
    {
        if(haveNetworkConnection() && firebaseUser!=null)
        {
            Map<String, Object> userProfile = new HashMap<>();
            userProfile.put("user_recent_online", Timestamp.now());
            firebaseFirestore.collection("users")
                    .document(firebaseUser.getUid())
                    .update(userProfile);
        }

    }

    private void AdsAuthorizationTimer()
    {
        final Handler handler = new Handler();

        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
               AdsSetVisibility();
            }
        };
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                handler.post(runnable);
            }
        }, 2000);

    }
    private void AdsSetVisibility()
    {
        if(!adsAuthorization.isBannerAdsEnabled())
        {
            adContainer.setVisibility(View.GONE);
        }
        else
        {
            adView.loadAd(adRequest);
        }
    }

    private void OnstartPriority()
    {
        if (firebaseUser == null) {

            Intent intent = new Intent(MainActivity.this, StartActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            finish();
        }
    }

    private void AnimationHandlerInitiate()
    {
        final Handler handler = new Handler();

        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                FlyBirdAnimationStart();
                rewardVideoImageAnimationStart();
            }
        };
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                handler.post(runnable);
            }
        }, 10, 100);

    }
    private void FlyBirdAnimationStart()
    {
            imageBird.setBackgroundResource(bird[currentFrame]);
            currentFrame++;
            if(currentFrame==7)
                currentFrame=0;
    }
    private void rewardVideoImageAnimationStart()
    {
        rewardVideoImage.setBackgroundResource(rewardVdImg[currentFrameRewardVDImg]);
        currentFrameRewardVDImg++;
        if(currentFrameRewardVDImg==3)
            currentFrameRewardVDImg=0;
    }

    public void startGame(View view)
    {
        if(!haveNetworkConnection()) // If Not Connected to Internet
        {
            noInternetDialog.setContentView(R.layout.popup_nointernet);
            itsOkayButton = noInternetDialog.findViewById(R.id.okayButton);
            turnOnButton = noInternetDialog.findViewById(R.id.turnOnButton);
            turnOnButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    noInternetDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Turn On Internet Service",Toast.LENGTH_LONG).show();
                }
            });
            itsOkayButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    noInternetDialog.dismiss();
                    gameInit();
                }
            });
            noInternetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            noInternetDialog.show();
            noInternetDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    Toast.makeText(getApplicationContext(), "Turn On Internet Service",Toast.LENGTH_LONG).show();
                }
            });
        }
        else
        {
            gameInit();
        }

    }

    public void gameInit()
    {
        //Log.i("ImageButton","clicked");
        Intent intent = new Intent(this, GameActivity.class);
        startActivity(intent);
        finish();
    }

    public void informationSupportActivityInit(View view)
    {
        Intent intent = new Intent(this, InformationSupport.class);
        startActivity(intent);
    }

    public void rateGame(View view)
    {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + this.getPackageName())));
        }
        catch (Exception e)
        { }
    }

 /*   public void shopItems(View view)
    {
        Toast.makeText(MainActivity.this, "Shop Items development in-Progress", Toast.LENGTH_SHORT).show();
    }
*/
    public void withdraw(View view)
    {
        if(!haveNetworkConnection()) // If Not Connected to Internet
        {
            noInternetDialog.setContentView(R.layout.popup_nointernet_withdraw);
            noInternetTurnOnButtonWithdraw = noInternetDialog.findViewById(R.id.noInternetTurnOnButtonWithdraw);
            closeButton = noInternetDialog.findViewById(R.id.closeButton);
            noInternetTurnOnButtonWithdraw.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    noInternetDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Turn On Internet Service",Toast.LENGTH_LONG).show();
                }
            });
            closeButton.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    noInternetDialog.dismiss();
                }
            });
            noInternetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            noInternetDialog.show();
            noInternetDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialogInterface) {
                    Toast.makeText(getApplicationContext(), "Turn On Internet Service",Toast.LENGTH_LONG).show();
                }
            });
        }
        else
        {
            //Toast.makeText(MainActivity.this, "Withdraw coins development in-Progress", Toast.LENGTH_SHORT).show();
          //  Intent intent = new Intent(this, WithdrawActivity.class);
            Intent intent = new Intent(this, WithdrawMethod.class);
            startActivity(intent);
        }

    }

    public void shareGame(View view)
    {
        try {
             openShareIntent(this,null,"Hey, look what I found, an exciting game. Download, play and earn!!\n"+ Uri.parse("https://play.google.com/store/apps/details?id=" + this.getPackageName()));
        }
        catch (Exception e)
        { }
    }

    public void openShareIntent(Context context, @Nullable View itemview, String shareText) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        if (itemview != null) {
            try {
                Uri imageUri = getImageUri(context, itemview, "postBitmap.jpeg");
                intent.setType("image/*");
                intent.putExtra(Intent.EXTRA_STREAM, imageUri);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            } catch (IOException e) {
                intent.setType("text/plain");
                e.printStackTrace();
            }
        } else {
            intent.setType("text/plain");
        }
        intent.putExtra(Intent.EXTRA_TEXT, shareText);
        startActivity(Intent.createChooser(intent, "Share Via:"));
    }

    private static Uri getImageUri(Context context, View view, String fileName) throws IOException {
        Bitmap bitmap = loadBitmapFromView(view);
        File pictureFile = new File(context.getExternalCacheDir(), fileName);
        FileOutputStream fos = new FileOutputStream(pictureFile);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, fos);
        fos.close();
        return Uri.parse("file://" + pictureFile.getAbsolutePath());
    }

    private static Bitmap loadBitmapFromView(View v) {
        v.clearFocus();
        v.setPressed(false);

        boolean willNotCache = v.willNotCacheDrawing();
        v.setWillNotCacheDrawing(false);

        // Reset the drawing cache background color to fully transparent
        // for the duration of this operation
        int color = v.getDrawingCacheBackgroundColor();
        v.setDrawingCacheBackgroundColor(0);

        if (color != 0) {
            v.destroyDrawingCache();
        }
        v.buildDrawingCache();
        Bitmap cacheBitmap = v.getDrawingCache();
        if (cacheBitmap == null) {
            v.setDrawingCacheEnabled(true);
            return v.getDrawingCache();
        }

        Bitmap bitmap = Bitmap.createBitmap(cacheBitmap);

        // Restore the view
        v.destroyDrawingCache();
        v.setWillNotCacheDrawing(willNotCache);
        v.setDrawingCacheBackgroundColor(color);

        return bitmap;
    }

    private boolean haveNetworkConnection()
    {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }



    @SuppressLint("SetTextI18n")
    public void displayRewardedVideo(View view)
    {

        //show WatchNow Dialog
        rewardedVideoDialog.setContentView(R.layout.popup_rewarded_video);
        watchNowButton = rewardedVideoDialog.findViewById(R.id.watchNowButton);
        RVCurrentCountTextView = rewardedVideoDialog.findViewById(R.id.RVCurrentCountTextView);

        RVCurrentCountTextView.setText(rewardVdCurrentCount +"/"+ rewardVdLimit_RemoteConfig);

        watchNowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckRVLimit();
            }
        });

        rewardedVideoDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        rewardedVideoDialog.show();

    }

    private void CheckRVLimit()
    {
        // if limit not reached Load and display ad
        if(rewardVdCurrentCount < rewardVdLimit_RemoteConfig)
        {
            dialog.setMessage("Loading Video...");
            dialog.setCancelable(false);
            dialog.show();
            loadRewardedVideoAd();
        }
        else
        {
            //limit reached - display dialog
            Toast.makeText(getApplicationContext(),"Limit Reached, Try again Later!",Toast.LENGTH_LONG).show();
        }

    }

    RewardedAdCallback adCallback = new RewardedAdCallback() {
        @Override
        public void onRewardedAdOpened() {
            // Ad opened.
        }

        @Override
        public void onRewardedAdClosed() {
            // Ad closed.
        }

        @Override
        public void onUserEarnedReward(@NonNull RewardItem reward) {
            // User earned reward.
            Log.d(TAG, "Rewarded video completed!");
            giveReward();
            clearMemory();
        }

        @Override
        public void onRewardedAdFailedToShow(AdError adError) {
            // Ad failed to display.
            Log.e(TAG, "Rewarded video ad failed to load: " + adError.getMessage());
            dialog.dismiss();
            rewardedVideoDialog.dismiss();
            Toast.makeText(getApplicationContext(),"No Video Available Currently, Try Later!", Toast.LENGTH_LONG).show();
            clearMemory();
        }
    };

    RewardedAdLoadCallback adLoadCallback = new RewardedAdLoadCallback() {
        @Override
        public void onRewardedAdLoaded() {
            // Ad successfully loaded.
            if (rewardedVideoAd.isLoaded()){
                // Rewarded video ad is loaded and ready to be displayed
                Log.d(TAG, "Rewarded video ad is loaded and ready to be displayed!");
                dialog.dismiss();
                rewardedVideoDialog.dismiss();
                rewardedVideoAd.show(MainActivity.this, adCallback);
            }
        }

        @Override
        public void onRewardedAdFailedToLoad(LoadAdError adError) {
            Log.d(TAG, "onRewardedAdFailedToLoad: ");
        }
    };

    private void loadRewardedVideoAd()
    {

        rewardedVideoAd = new RewardedAd(this, getResources().getString(R.string.admob_rewarded_video));
        rewardedVideoAd.loadAd(new AdRequest.Builder().build(), adLoadCallback);
    }

    private void giveReward()
    {
        dialog.setMessage("Processing Coins...");
        dialog.setCancelable(false);
        dialog.show();

        rewardVdCurrentCount+=1;


        //getting user coins
        firebaseFirestore.collection("users")
                .document(firebaseUser.getUid())
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                        if(task.isSuccessful())
                        {
                            String coins_temp = task.getResult().get("user_earned_coins")+"";
                            if(coins_temp!=null)
                            {
                                float earned_coins = Float.parseFloat(coins_temp);
                                earned_coins +=1;

                                Map<String, Object> userProfile_updateCoins = new HashMap<>();
                                userProfile_updateCoins.put("user_earned_coins",Float.toString(earned_coins));
                                userProfile_updateCoins.put("daily_reward_count",Integer.toString(rewardVdCurrentCount));

                                //updating with latest coins and video reward count
                                firebaseFirestore.collection("users")
                                        .document(firebaseUser.getUid())
                                        .update(userProfile_updateCoins);

                                displayConfirmationUpdateUI();
                            }
                        }
                    }
                });
    }

    private void displayConfirmationUpdateUI()
    {
        dialog.dismiss();

        //show WatchNow Dialog
        coinsAddSuccessDialog.setContentView(R.layout.popup_rewardvideo_coins_success);
        Button okay = coinsAddSuccessDialog.findViewById(R.id.okayButton);

        okay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                coinsAddSuccessDialog.dismiss();
            }
        });

        coinsAddSuccessDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        coinsAddSuccessDialog.setCancelable(false);
        coinsAddSuccessDialog.show();

    }

    private void clearMemory()
    {
        if (rewardedVideoAd != null) {
            rewardedVideoAd = null;
        }
    }

    public void leaderBoardClicked(View view)
    {
        if(haveNetworkConnection())
        {
            Intent intent= new Intent(MainActivity.this, LeaderBoard.class);
            startActivity(intent);
        }
        else
        {
            Toast.makeText(getApplicationContext(),"Turn On Internet Service!",Toast.LENGTH_LONG).show();
        }
    }

    public void ProfileEditActivityInit(View view)
    {
        if(haveNetworkConnection())
        {
            Intent intent= new Intent(MainActivity.this, ProfileEdit.class);
            startActivity(intent);
        }
        else
        {
            Toast.makeText(getApplicationContext(),"Turn On Internet Service!",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onResume()
    {
        remoteConfigCheck();
        super.onResume();
    }

    @Override
    protected void onDestroy()
    {
        if (adView != null) {
            adView.destroy();
        }
        if (rewardedVideoAd != null) {
            rewardedVideoAd = null;
        }
        super.onDestroy();
    }
}
