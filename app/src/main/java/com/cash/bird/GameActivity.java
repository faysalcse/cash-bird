package com.cash.bird;

import android.app.Activity;
import android.os.Bundle;
import androidx.annotation.Nullable;

import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;

import java.util.Timer;
import java.util.TimerTask;

public class GameActivity extends Activity {

    GameView gameView;
    AdsAuthorization adsAuthorization;
    Timer timer;
    LinearLayout adContainer;
    private AdView mAdView;
    private AdRequest adRequest;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        AppConstants.gameActivityContext = this;

        MobileAds.initialize(this);

        mAdView = new AdView(this);
        mAdView.setAdSize(AdSize.SMART_BANNER);
        // Replace this with your ad unit Id
        mAdView.setAdUnitId(getResources().getString(R.string.admob_banner));
        // Create an ad request.
        adRequest = new AdRequest.Builder().build();
        // Start loading the ad in the background.
        mAdView.loadAd(adRequest);



        adsAuthorization = new AdsAuthorization(this);


        mAdView.setAdListener(new AdListener(){
            @Override
            public void onAdClosed() {
                super.onAdClosed();
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                mAdView.loadAd(adRequest);
            }

            @Override
            public void onAdOpened() {
                super.onAdOpened();
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
            }


        });


        AdsAuthorizationTimer();

        RelativeLayout layout = new RelativeLayout(this);
        layout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        gameView = new GameView(this); // Instantiate gameView
        // Define ad view parameters
        RelativeLayout.LayoutParams adParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        adParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        adParams.addRule(RelativeLayout.CENTER_HORIZONTAL);
        layout.addView(gameView);
        layout.addView(mAdView, adParams);

        // Set main renderer
        setContentView(layout);
    }

    private void AdsAuthorizationTimer()
    {
        final Handler handler = new Handler();

        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                AdsSetVisibility();
            }
        };
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                handler.post(runnable);
            }
        }, 3500);

    }
    private void AdsSetVisibility()
    {
        if(!adsAuthorization.isBannerAdsEnabled())
        {
             adContainer.setVisibility(View.GONE);
        }
        else
        {
            mAdView.loadAd(adRequest);
        }
    }

    @Override
    protected void onDestroy()
    {
        if (mAdView != null) {
            mAdView.destroy();
        }
        super.onDestroy();
    }
}
