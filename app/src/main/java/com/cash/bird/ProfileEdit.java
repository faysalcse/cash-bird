package com.cash.bird;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;


public class ProfileEdit extends Activity
{
    public static final String TAG = "ProfileEdit";

    EditText editTextFirstName;
    EditText editTextLastName;
    EditText editTextUserEmail;
    EditText editTextUserMobile;
    AutoCompleteTextView editTextUserCountry;
    RadioButton radioButtonUserMale;
    RadioButton radioButtonUserFemale;
    Button buttonSave;

    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private FirebaseFirestore firebaseFirestore;

    ProgressDialog dialog;
    private InterstitialAd interstitialAd;

    /*
    String profile_firstName;
    String profile_lastname;
    String profile_email;
    String profile_mobile;
    String profile_country;
    String profile_gender;
    private boolean changesMade = false;
    */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_edit);
        MobileAds.initialize(this);

        editTextFirstName = findViewById(R.id.editTextFirstName);
        editTextLastName = findViewById(R.id.editTextLastName);
        editTextUserEmail = findViewById(R.id.editTextUserEmail);
        editTextUserMobile = findViewById(R.id.editTextUserMobile);
        editTextUserCountry = findViewById(R.id.editTextUserCountry);
        radioButtonUserMale = findViewById(R.id.radioButtonUserMale);
        radioButtonUserFemale = findViewById(R.id.radioButtonUserFemale);

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        firebaseFirestore = FirebaseFirestore.getInstance();

        String[] countries_list = getResources().getStringArray(R.array.countries_array);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, countries_list);
        editTextUserCountry.setAdapter(adapter);

        dialog = new ProgressDialog(ProfileEdit.this);
        dialog.setMessage("Loading Profile...");
        dialog.setCancelable(false);
        dialog.show();
        loadInterstitial();
        firebaseConnectAndUpdate();
    }

    private void loadInterstitial()
    {
        //load interstitial ad here
        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(getString(R.string.admob_banner));
        interstitialAd.setAdListener(new AdListener() {
        @Override
        public void onAdLoaded() {
            // Code to be executed when an ad finishes loading.
            interstitialAd.show();
        }

        @Override
        public void onAdFailedToLoad(LoadAdError adError) {
            // Code to be executed when an ad request fails.
        }

        @Override
        public void onAdOpened() {
            // Code to be executed when the ad is displayed.
        }

        @Override
        public void onAdClicked() {
            // Code to be executed when the user clicks on an ad.
        }

        @Override
        public void onAdLeftApplication() {
            // Code to be executed when the user has left the app.
        }

        @Override
        public void onAdClosed() {
            // Code to be executed when the interstitial ad is closed.
            interstitialAd.loadAd(new AdRequest.Builder().build());
        }
    });

   interstitialAd.loadAd(new AdRequest.Builder().build());

    }

    private void firebaseConnectAndUpdate()
    {
        firebaseFirestore.collection("users")
                .document(firebaseUser.getUid())
                .get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {

                        if(task.isSuccessful())
                        {
                            String firstName = task.getResult().getString("user_firstname");
                            String lastname = task.getResult().getString("user_lastname");
                            String email = task.getResult().getString("user_email");
                            String mobile = task.getResult().getString("user_mobile");
                            String country = task.getResult().getString("user_country");
                            String gender = task.getResult().getString("user_gender");

                            onLoadUpdateUI(firstName,lastname,email,mobile,country,gender);
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getApplicationContext(),"Something went wrong, Try again!",Toast.LENGTH_LONG).show();
                        ProfileEdit.this.finish();
                    }
                });
    }

    private void onLoadUpdateUI(String firstName, String lastname, String email, String mobile, String country, String gender)
    {
/*
        profile_firstName = firstName;
        profile_lastname = lastname;
        profile_email = email;
        profile_mobile = mobile;
        profile_country = country;
        profile_gender = gender;
*/
        if(firstName!=null)
        {
            editTextFirstName.setText(firstName);
        }
        if(lastname!=null)
        {
            editTextLastName.setText(lastname);
        }
        if(email!=null)
        {
            editTextUserEmail.setText(email);
        }
        if(mobile!=null)
        {
            editTextUserMobile.setText(mobile);
        }
        if(country!=null)
        {
            editTextUserCountry.setText(country);
        }
        if(gender!=null)
        {
            if(gender.equalsIgnoreCase("male"))
            {
                radioButtonUserMale.setChecked(true);
            }
            else if(gender.equalsIgnoreCase("female"))
            {
                radioButtonUserFemale.setChecked(true);
            }
        }
        dialog.dismiss();
    }

    public void saveButtonClicked(View view)
    {
        if(!checkForPrimaryFieldNullEntries()) // FirstName, LastName, Email
        {
            String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
            if(editTextUserEmail.getText().toString().matches(emailPattern))
            {
                dialog.setMessage("Updating Profile...");
                dialog.show();

                String updatedMobileNumber = editTextUserMobile.getText().toString();
                String updatedCountry = editTextUserCountry.getText().toString();
                String updatedGender = "";
                if(radioButtonUserMale.isChecked())
                {
                    updatedGender = "male";
                }
                if(radioButtonUserFemale.isChecked())
                {
                    updatedGender = "female";
                }

                Map<String, Object> userProfile = new HashMap<>();
                userProfile.put("user_firstname", editTextFirstName.getText().toString());
                userProfile.put("user_lastname", editTextLastName.getText().toString());
                userProfile.put("user_email", editTextUserEmail.getText().toString());
                userProfile.put("user_mobile", updatedMobileNumber);
                userProfile.put("user_country", updatedCountry);
                userProfile.put("user_gender", updatedGender);

                firebaseFirestore.collection("users")
                        .document(firebaseUser.getUid())
                        .update(userProfile)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if(task.isSuccessful())
                                {
                                    Toast.makeText(getApplicationContext(),"Profile Updated Successfully!", Toast.LENGTH_LONG).show();
                                    dialog.dismiss();
                                    ProfileEdit.this.finish();
                                }
                                else
                                {
                                    Toast.makeText(getApplicationContext(),"Something went wrong, check your internet & try again.", Toast.LENGTH_LONG).show();
                                    dialog.dismiss();
                                }
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Toast.makeText(getApplicationContext(),"Something went wrong, check your internet & try again.", Toast.LENGTH_LONG).show();
                                dialog.dismiss();
                            }
                        });
            }
            else
            {
                Toast.makeText(getApplicationContext(),"Invalid Email Address..",Toast.LENGTH_LONG).show();
            }
        }
    }

    private boolean checkForPrimaryFieldNullEntries()
    {
        if(editTextFirstName.getText().toString().isEmpty())
        {
            Toast.makeText(getApplicationContext(),"FirstName Can't be Empty",Toast.LENGTH_LONG).show();
            return true;
        }
        if(editTextLastName.getText().toString().isEmpty())
        {
            Toast.makeText(getApplicationContext(),"LastName Can't be Empty",Toast.LENGTH_LONG).show();
            return true;
        }
        if(editTextUserEmail.getText().toString().isEmpty())
        {
            Toast.makeText(getApplicationContext(),"Primary Email Can't be Empty",Toast.LENGTH_LONG).show();
            return true;
        }
        return false;
    }

    @Override
    protected void onDestroy()
    {
        ProfileEdit.this.finish();
        super.onDestroy();
    }
}
