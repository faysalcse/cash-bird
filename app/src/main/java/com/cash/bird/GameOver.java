package com.cash.bird;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
//import android.support.annotation.Nullable;
//import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;


public class GameOver extends Activity{

    public static final String TAG = GameOver.class.getSimpleName();

    private AdView adView;
    private InterstitialAd interstitialAd;
    TextView tvScore, tvPersonalBest, earnedCoinsHeadingTextView,earnedCoinsTextView;
    int score;
    float earnedCoins;
    String EarnedCoins_ = "0";
    private Timer timer;
    boolean retry_enable_Ads;
    boolean firebaseSaved_retry_enable;
    AsyncTaskRunner asyncTaskRunner;
    NumberFormat numberFormat;
    Dialog noInternetDialog;
    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private FirebaseFirestore firebaseFirestore;
    private AdsAuthorization adsAuthorization;
    static int scoreLimit = 8; //if score below this then interstitial ad won't show
    LinearLayout adContainer;
    private FirebaseRemoteConfig firebaseRemoteConfig;
    private HashMap<String, Object> firebaseDefaults;
    private final String EARNEED_COINS_HEADING_TEXT = "no_earned_coins_heading";
    private AdRequest adRequest;

    private  LinearLayout retryLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_over);

        retryLayout = findViewById(R.id.retryLayout);

        retry_enable_Ads = false;
        firebaseSaved_retry_enable = false;

        adsAuthorization = new AdsAuthorization(this);

        asyncTaskRunner= new AsyncTaskRunner();


        // Initialize the Audience Network SDK
        MobileAds.initialize(this);

        adView = findViewById(R.id.adView);
        adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);


        noInternetDialog = new Dialog(this);

        if(!adsAuthorization.isBannerAdsEnabled())
        {
            adView.setVisibility(View.GONE);
        }

        //load interstitial ad here

        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId(getResources().getString(R.string.admob_interstitial));
        interstitialAd.setAdListener(new AdListener(){
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
                interstitialAd.show();
            }

            @Override
            public void onAdFailedToLoad(LoadAdError adError) {
                // Code to be executed when an ad request fails.
                retryLayout.setVisibility(View.VISIBLE);

                retry_enable_Ads=true;
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the interstitial ad is closed.
                retryLayout.setVisibility(View.VISIBLE);
                retry_enable_Ads =true;
            }
        });

        interstitialAd.loadAd(new AdRequest.Builder().build());


        score = getIntent().getExtras().getInt("score");
        AdsHandlerInitiate();
        SharedPreferences pref = getSharedPreferences("MyPref",0);
        int scoreSP = pref.getInt("scoreSP",0);
        SharedPreferences.Editor editor = pref.edit();
        if(score > scoreSP){
            scoreSP = score;
            editor.putInt("scoreSP", scoreSP);
            editor.commit();
        }

        numberFormat = NumberFormat.getNumberInstance();
        numberFormat.setMinimumFractionDigits(2);
        numberFormat.setMaximumFractionDigits(2);

        // earnedCoins = (float) score/4;
        calculateEarnedCoins(score);

        tvScore = findViewById(R.id.tvScore);
        tvPersonalBest = findViewById(R.id.tvPersonalBest);
        earnedCoinsHeadingTextView = findViewById(R.id.earnedCoinsHeadingTextView);
        earnedCoinsTextView = findViewById(R.id.earnedCoinsTextView);

        tvScore.setText(""+score);
        tvPersonalBest.setText(""+scoreSP);
        earnedCoinsTextView.setText(EarnedCoins_);

        if(score!=0)
        {
            if (score > 5)
            {
                if (haveNetworkConnection())
                {
                    saveScoreAndCoins(scoreSP, EarnedCoins_);
                    earnedCoinsHeadingTextView.setText("Earned Coins");
                }
                else
                    {
                    displayCoinsNotSavedDialog();
                        earnedCoinsHeadingTextView.setVisibility(View.GONE);
                        earnedCoinsTextView.setVisibility(View.GONE);
                }
            }
            else // score <= 5
            {
                earnedCoinsHeadingTextView.setTextSize(12);
                earnedCoinsHeadingTextView.setPadding(1,5,1,5);
                earnedCoinsHeadingTextView.setText(" Coins get counted when score >5 ");
                //earnedCoinsHeadingTextView.setVisibility(View.GONE);
                earnedCoinsTextView.setVisibility(View.GONE);
            }
        }
        else // score == 0
        {
            earnedCoinsHeadingTextView.setVisibility(View.GONE);
            earnedCoinsTextView.setVisibility(View.GONE);
        }
    }

    private void remoteConfigCheck()
    {
        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        firebaseDefaults = new HashMap<>();
        firebaseDefaults.put(EARNEED_COINS_HEADING_TEXT," Coins get counted when score >5 ");

        firebaseRemoteConfig.setDefaults(firebaseDefaults);
        firebaseRemoteConfig.setConfigSettings(new FirebaseRemoteConfigSettings.Builder().setDeveloperModeEnabled(true).build());

        firebaseRemoteConfig.fetch().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful())
                {
                    firebaseRemoteConfig.activateFetched();
                    checkForUpdateParameters();
                }
            }
        });
    }

    private void checkForUpdateParameters()
    {
        String text =  firebaseRemoteConfig.getString(EARNEED_COINS_HEADING_TEXT);
        if(score<=5)
        earnedCoinsHeadingTextView.setText(text);
        else
            earnedCoinsHeadingTextView.setText("Earned Coins");
    }

    private void displayCoinsNotSavedDialog()
    {
        noInternetDialog.setContentView(R.layout.popup_nointernet_nocoins_saved);
        Button okayButton = noInternetDialog.findViewById(R.id.okayButton);
        okayButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                noInternetDialog.dismiss();
            }
        });
        noInternetDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        noInternetDialog.setCancelable(false);
        noInternetDialog.show();
        noInternetDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Toast.makeText(getApplicationContext(), "Turn On Internet Service",Toast.LENGTH_LONG).show();
            }
        });

    }

    private void calculateEarnedCoins(int score)
    {
        if(score<=5)
        {
            earnedCoins= 0;
            EarnedCoins_ = numberFormat.format(earnedCoins);
        }
        else if(score <= 50)
        {
            earnedCoins = (float) score / 16;
            EarnedCoins_ = numberFormat.format(earnedCoins);
        }
        else if(score <= 100)
        {
            float score_till_50 = (float) 50/16; //3.125

            float temp_holder_50_100 = score - 50;
            temp_holder_50_100 = temp_holder_50_100 / 8;

            earnedCoins = score_till_50 + temp_holder_50_100;

            EarnedCoins_ = numberFormat.format(earnedCoins);

        }
        else
        {
            float score_till_50 = (float) 50/16; //3.125
            float score_till_50_100= (float) 50/8; //6.25

            float temp_holder_above100_score = score - 100;
            temp_holder_above100_score = temp_holder_above100_score / 4;

            earnedCoins = score_till_50 + score_till_50_100 + temp_holder_above100_score;

            EarnedCoins_ = numberFormat.format(earnedCoins);


        }
    }

    private void saveScoreAndCoins(int personalBest, String earnedCoins)
    {
        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        firebaseFirestore = FirebaseFirestore.getInstance();
        asyncTaskRunner.execute(Integer.toString(personalBest), earnedCoins);

    }

    private void AdsHandlerInitiate()
    {
        final Handler handler = new Handler();

        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                displayInterstitialAd(score);
            }
        };
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {

                handler.post(runnable);
            }
        }, 2000);

    }

    public void displayInterstitialAd(int score)
    {
        if(adsAuthorization.getScore_limit()!=0)
        {
            scoreLimit = adsAuthorization.getScore_limit();
        }
        if(score>=scoreLimit)
        {
            if(adsAuthorization.isInterstitialAdsEnabled())
            {
                if(interstitialAd == null || !interstitialAd.isLoaded()) {
                    return;
                }
                // Check if ad is already expired or invalidated, and do not show ad if that is the case. You will not get paid to show an invalidated ad.
                // Show the ad
                interstitialAd.show();

                /*
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
                    Log.d("TAG", "The interstitial wasn't loaded yet.");
                }
                 */
            }
            else
            {
                retry_enable_Ads=true;
            }
        }
        if(!adsAuthorization.isBannerAdsEnabled())
        {
            adView.setVisibility(View.GONE);
        }
        else
        {
            adView.loadAd(adRequest);
        }
    }

    public void home(View view)
    {
        Intent intent = new Intent(GameOver.this, MainActivity.class);
        startActivity(intent);
        GameOver.this.finish();
    }

    public void retry(View view)
    {
        if(firebaseSaved_retry_enable) // if Internet Connected and coins save successful
        {
            if(score < scoreLimit)
            {
                AppConstants.initialization(this.getApplicationContext());
                Intent intent = new Intent(GameOver.this, GameActivity.class);
                startActivity(intent);
                GameOver.this.finish();
            }
            else if(retry_enable_Ads)
            {
                AppConstants.initialization(this.getApplicationContext());
                Intent intent = new Intent(GameOver.this, GameActivity.class);
                startActivity(intent);
                GameOver.this.finish();
            }
        }
        else if(score < scoreLimit || retry_enable_Ads ) // If No Internet
        {
            AppConstants.initialization(this.getApplicationContext());
            Intent intent = new Intent(GameOver.this, GameActivity.class);
            startActivity(intent);
            GameOver.this.finish();
        }

    }

    public void exit(View view)
    {

/*        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Log.d("TAG", "The interstitial wasn't loaded yet.");
        }
 */
        finish();
    }

    private class AsyncTaskRunner extends AsyncTask<String, String, String>
    {
        private String resp;
        ProgressDialog progressDialog;

        @Override
        protected String doInBackground(String... params) {
            //publishProgress("Sleeping..."); // Calls onProgressUpdate()
            try
            {
                final int personalBest = Integer.parseInt(params[0]);
                final float EarnedCoins = Float.parseFloat(params[1]);

                firebaseFirestore.collection("users")
                        .document(firebaseUser.getUid())
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                                if(task.isSuccessful())
                                {
                                   // int fireBasePersonal = Integer.parseInt(task.getResult().getString("user_personal_best"));
                                    float fireBaseEarnedCoins = Float.parseFloat(task.getResult().getString("user_earned_coins"));

                                    float totalEarned = fireBaseEarnedCoins+EarnedCoins;

                                    final Map<String, Object> userProfile_update = new HashMap<>();
                                    userProfile_update.put("user_personal_best",Integer.toString(personalBest));
                                    userProfile_update.put("user_earned_coins",Float.toString(totalEarned));

                                    firebaseFirestore.collection("users")
                                            .document(firebaseUser.getUid())
                                            .update(userProfile_update).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if(task.isSuccessful())
                                            {
                                                resp="success";
                                            }
                                        }
                                    });

                                }
                            }
                        });
            }
            catch (Exception e)
            {
                e.printStackTrace();
                resp = e.getMessage();
            }
            return resp;
        }


        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
            progressDialog.dismiss();
            firebaseSaved_retry_enable = true;
           // Toast.makeText(getApplicationContext(),result,Toast.LENGTH_SHORT).show();
           // finalResult.setText(result);
        }

        @Override
        protected void onPreExecute() {
            progressDialog = new ProgressDialog(GameOver.this);
            progressDialog.setMessage("Processing...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected void onProgressUpdate(String... text) {
            //finalResult.setText(text[0]);
        }
    }

    private boolean haveNetworkConnection()
    {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    @Override
    protected void onResume() {
        remoteConfigCheck();
        super.onResume();
    }

    @Override
    protected void onDestroy()
    {
        GameOver.this.finish();
        super.onDestroy();
    }
}
