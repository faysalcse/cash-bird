package com.cash.bird;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

public class WithdrawMethod extends AppCompatActivity {

    private RadioButton paypal;
    private RadioButton google_gift_card;
    private RadioButton garena_payment;
    private RadioButton cashu_payment;
    private RadioButton pubg_payment;
    private RadioButton itunes_payment;
    private Button nextBtn;
    private String method = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_withdraw_method);
        MobileAds.initialize(this);
        getSupportActionBar().setTitle("Choose Withdraw Option");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        paypal = findViewById(R.id.paypal);
        google_gift_card = findViewById(R.id.google_gift_card);
        garena_payment = findViewById(R.id.garena_payment);
        cashu_payment = findViewById(R.id.cashu_payment);
        pubg_payment = findViewById(R.id.pubg_payment);
        itunes_payment = findViewById(R.id.itunes_payment);
        nextBtn = findViewById(R.id.nextBtn);

        paypal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method = "PayPal";
                paypal.setChecked(true);
                google_gift_card.setChecked(false);
                garena_payment.setChecked(false);
                cashu_payment.setChecked(false);
                pubg_payment.setChecked(false);
                itunes_payment.setChecked(false);
            }
        });
        google_gift_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method = "Google";
                google_gift_card.setChecked(true);
                paypal.setChecked(false);
                garena_payment.setChecked(false);
                cashu_payment.setChecked(false);
                pubg_payment.setChecked(false);
                itunes_payment.setChecked(false);
            }
        });
        garena_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method = "Garena";
                garena_payment.setChecked(true);
                paypal.setChecked(false);
                google_gift_card.setChecked(false);
                cashu_payment.setChecked(false);
                pubg_payment.setChecked(false);
                itunes_payment.setChecked(false);
            }
        });
        cashu_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method = "Cashu";
                cashu_payment.setChecked(true);
                garena_payment.setChecked(false);
                paypal.setChecked(false);
                google_gift_card.setChecked(false);
                pubg_payment.setChecked(false);
                itunes_payment.setChecked(false);
            }
        });
        pubg_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method = "PUBG";
                cashu_payment.setChecked(false);
                garena_payment.setChecked(false);
                paypal.setChecked(false);
                google_gift_card.setChecked(false);
                pubg_payment.setChecked(true);
                itunes_payment.setChecked(false);
            }
        });
        itunes_payment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                method = "iTunes";
                cashu_payment.setChecked(false);
                garena_payment.setChecked(false);
                paypal.setChecked(false);
                google_gift_card.setChecked(false);
                pubg_payment.setChecked(false);
                itunes_payment.setChecked(true);
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (method == null || TextUtils.isEmpty(method)) {
                    Toast.makeText(WithdrawMethod.this, "Please select a payment option !", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(WithdrawMethod.this, WithdrawActivity.class);
                intent.putExtra("method",method);
                startActivity(intent);
            }
        });


        AdView adView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}