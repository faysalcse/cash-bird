package com.cash.bird;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import java.util.ArrayList;

public class LeaderBoard extends Activity
{
    private FirebaseRemoteConfig firebaseRemoteConfig;
    ListView mListView;
    TextView leaderBoardHeading;
    TextView leaderBoardFooter;
    ProgressDialog dialog;
    private AdView adView;
    LinearLayout adContainer;
    ScrollView scrollViewParent;

    private final String LeaderBoardHeading = "leader_board_heading";
    private final String LeaderBoardFooter = "leader_board_footer";

    private final String player1_name = "player1_name";
    private final String player1_score = "player1_score";
    private final String player1_payouts = "player1_payouts";

    private final String player2_name = "player2_name";
    private final String player2_score = "player2_score";
    private final String player2_payouts = "player2_payouts";

    private final String player3_name = "player3_name";
    private final String player3_score = "player3_score";
    private final String player3_payouts = "player3_payouts";

    private final String player4_name = "player4_name";
    private final String player4_score = "player4_score";
    private final String player4_payouts = "player4_payouts";

    private final String player5_name = "player5_name";
    private final String player5_score = "player5_score";
    private final String player5_payouts = "player5_payouts";

    private final String player6_name = "player6_name";
    private final String player6_score = "player6_score";
    private final String player6_payouts = "player6_payouts";

    private final String player7_name = "player7_name";
    private final String player7_score = "player7_score";
    private final String player7_payouts = "player7_payouts";

    private final String player8_name = "player8_name";
    private final String player8_score = "player8_score";
    private final String player8_payouts = "player8_payouts";

    private final String player9_name = "player9_name";
    private final String player9_score = "player9_score";
    private final String player9_payouts = "player9_payouts";

    private final String player10_name = "player10_name";
    private final String player10_score = "player10_score";
    private final String player10_payouts = "player10_payouts";
    private AdRequest adRequest;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaderboard);
        mListView = findViewById(R.id.gemUserList);
        leaderBoardHeading = findViewById(R.id.leaderBoardHeading);
        leaderBoardFooter = findViewById(R.id.leaderBoardFooter);
        scrollViewParent = findViewById(R.id.scrollViewParent);

        // Initialize the Audience Network SDK
        MobileAds.initialize(this);

        dialog = new ProgressDialog(LeaderBoard.this);
        dialog.setMessage("Loading...");
        dialog.setCancelable(false);
        dialog.show();

        scrollViewParent.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                findViewById(R.id.gemUserList).getParent().requestDisallowInterceptTouchEvent(false);
                return false;
            }
        });

        mListView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });

        loadBannerAd();
        remoteConfigCheck();

    }

    private void loadBannerAd()
    {

        adView = findViewById(R.id.adView);
        adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);
        adView.setAdListener(new AdListener(){
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.

            }

            @Override
            public void onAdFailedToLoad(LoadAdError adError) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when an ad opens an overlay that
                // covers the screen.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the user is about to return
                // to the app after tapping on an ad.
            }
        });

    }

    private void remoteConfigCheck()
    {
        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        firebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);
        firebaseRemoteConfig.setConfigSettings(new FirebaseRemoteConfigSettings.Builder().setDeveloperModeEnabled(true).build());

        firebaseRemoteConfig.fetch().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful())
                {
                    firebaseRemoteConfig.activateFetched();
                    checkForUpdates();
                }
            }
        });
    }


    private void checkForUpdates()
    {

        String heading = firebaseRemoteConfig.getString(LeaderBoardHeading);
        String footer = firebaseRemoteConfig.getString(LeaderBoardFooter);

        leaderBoardHeading.setText(heading);
        leaderBoardFooter.setText(footer);

        String userName1 = firebaseRemoteConfig.getString(player1_name);
        int score1 = (int) firebaseRemoteConfig.getDouble(player1_score);
        int checkout1 = (int) firebaseRemoteConfig.getDouble(player1_payouts);

        String userName2 = firebaseRemoteConfig.getString(player2_name);
        int score2 = (int) firebaseRemoteConfig.getDouble(player2_score);
        int checkout2 = (int) firebaseRemoteConfig.getDouble(player2_payouts);

        String userName3 = firebaseRemoteConfig.getString(player3_name);
        int score3 = (int) firebaseRemoteConfig.getDouble(player3_score);
        int checkout3 = (int) firebaseRemoteConfig.getDouble(player3_payouts);

        String userName4 = firebaseRemoteConfig.getString(player4_name);
        int score4 = (int) firebaseRemoteConfig.getDouble(player4_score);
        int checkout4 = (int) firebaseRemoteConfig.getDouble(player4_payouts);

        String userName5 = firebaseRemoteConfig.getString(player5_name);
        int score5 = (int) firebaseRemoteConfig.getDouble(player5_score);
        int checkout5 = (int) firebaseRemoteConfig.getDouble(player5_payouts);

        String userName6 = firebaseRemoteConfig.getString(player6_name);
        int score6 = (int) firebaseRemoteConfig.getDouble(player6_score);
        int checkout6 = (int) firebaseRemoteConfig.getDouble(player6_payouts);

        String userName7 = firebaseRemoteConfig.getString(player7_name);
        int score7 = (int) firebaseRemoteConfig.getDouble(player7_score);
        int checkout7 = (int) firebaseRemoteConfig.getDouble(player7_payouts);

        String userName8 = firebaseRemoteConfig.getString(player8_name);
        int score8 = (int) firebaseRemoteConfig.getDouble(player8_score);
        int checkout8 = (int) firebaseRemoteConfig.getDouble(player8_payouts);

        String userName9 = firebaseRemoteConfig.getString(player9_name);
        int score9 = (int) firebaseRemoteConfig.getDouble(player9_score);
        int checkout9 = (int) firebaseRemoteConfig.getDouble(player9_payouts);

        String userName10 = firebaseRemoteConfig.getString(player10_name);
        int score10 = (int) firebaseRemoteConfig.getDouble(player10_score);
        int checkout10 = (int) firebaseRemoteConfig.getDouble(player10_payouts);

        //Creating Objects of each player with Player Class

        Player player1 =new Player(1,userName1,score1,checkout1);
        Player player2 =new Player(2,userName2,score2,checkout2);
        Player player3 =new Player(3,userName3,score3,checkout3);
        Player player4 =new Player(4,userName4,score4,checkout4);
        Player player5 =new Player(5,userName5,score5,checkout5);
        Player player6 =new Player(6,userName6,score6,checkout6);
        Player player7 =new Player(7,userName7,score7,checkout7);
        Player player8 =new Player(8,userName8,score8,checkout8);
        Player player9 =new Player(9,userName9,score9,checkout9);
        Player player10 =new Player(10,userName10,score10,checkout10);

        //Add these objects to ArrayList Now

        ArrayList<Player> PlayerList = new ArrayList<>();
        PlayerList.add(player1);
        PlayerList.add(player2);
        PlayerList.add(player3);
        PlayerList.add(player4);
        PlayerList.add(player5);
        PlayerList.add(player6);
        PlayerList.add(player7);
        PlayerList.add(player8);
        PlayerList.add(player9);
        PlayerList.add(player10);

        //Create Custom Adapter
        PlayerListAdapter adapter = new PlayerListAdapter(this,R.layout.adapter_view_layout,PlayerList);
        mListView.setAdapter(adapter);

        dialog.dismiss();
    }


}
